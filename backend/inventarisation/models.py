from django.db import models
from django.utils.timezone import now

from .managers import (
    RoomManager,
    ItemManager
)


class Building(models.Model):
    """
    Дом
    """
    name = models.TextField()
    adress = models.TextField(
        null=True,
        blank=True
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Строение"
        verbose_name_plural = "Строения"


class Room(models.Model):
    """
    Комнаты, в которых хранятся вещи и контейнеры
    """
    name = models.TextField()
    building = models.ForeignKey(
        Building,
        on_delete=models.CASCADE,
        verbose_name="дом"
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Комната"
        verbose_name_plural = "Комнаты"


# class MainCategory(models.Model):
#     """ Является записью ведущих каталогов, которые дальше дробяться на категории """
#     name = models.TextField()
#
#     def __str__(self):
#         return str(self.name)
#
#     class Meta:
#         verbose_name = "Главная категория"
#         verbose_name_plural = "Главные категории"


class Category(models.Model):
    """
    Отображает к какой категории отосится вещь.
    Также определяет поля, которые будет иметь вещь
    """
    name = models.TextField()
    # main_category = models.ForeignKey(
    #     MainCategory,
    #     null=False,
    #     blank=False,
    #     on_delete=models.CASCADE
    # )

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Container(models.Model):
    """
    Неподвижные контейнеры, которые могут находиться в
    комнатах. Например: подоконники, вырезные полки,
    встраиваемые в стену сейфы.
    """
    name = models.TextField()
    building = models.ForeignKey(
        Building,
        on_delete=models.CASCADE,
        verbose_name='дом'
    )

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Контейнер"
        verbose_name_plural = "Контейнеры"


class Item(models.Model):
    """
    Предметы, которые могут выступать контейнерами и
    вещами, которые отслеживает приложение
    """

    name = models.TextField(
        verbose_name="Наименование"
    )
    category = models.ForeignKey(
        Category,
        models.CASCADE,
        verbose_name='Категория'
    )
    is_container = models.BooleanField(
        default=False,
        verbose_name='Это контейнер'
    )
    # хранится в другом предмете
    container_item = models.ForeignKey(
        'self',
        models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name='Другой контейнер (подвижный)',
        related_name='item_self'
    )
    # хранится в неподвижном контейнере
    container = models.ForeignKey(
        Container,
        models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name='Неподвижный контейнер'
    )
    # хранится в общем в комнате
    room = models.ForeignKey(
        Room,
        models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name='Комната'
    )
    price = models.DecimalField(
        max_digits=999999999999,
        decimal_places=10,
        default=0.00,
        verbose_name='Цена'
    )
    # Количество предметов, которые отображает запись
    count = models.IntegerField(
        verbose_name='Количество'
    )

    date_added = models.DateTimeField(
        default=now,
        null=True,
        blank=True,
        verbose_name="Дата добавления"
    )

    date_purchase = models.DateTimeField(
        default=None,
        null=True,
        blank=True,
        verbose_name="Дата покупки предмета"
    )

    objects = ItemManager()

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Предмет"
        verbose_name_plural = "Предметы"
        ordering = ['-date_added']


class ItemPhoto(models.Model):
    """
    Фото, которые отображают предмет
    """
    item = models.ForeignKey(
        Item,
        null=False,
        blank=False,
        on_delete=models.DO_NOTHING,
        verbose_name="Предмет",
        related_name='photos',
    )
    photo_file = models.FileField(
        verbose_name="Фото файл",
    )
    date_added = models.DateTimeField(
        default=now,
        null=True,
        blank=True,
        verbose_name="Дата добавления"
    )

    class Meta:
        verbose_name = "Фото"
        verbose_name_plural = "Фотографии"


class ItemDescriptionTemplate(models.Model):
    """
    Сохраняет определенный набор значений, которые должны
    быть заполнены для предмета с определенной категорией
    """
    key = models.TextField(
        verbose_name="Наименование параметра"
    )
    order_number = models.IntegerField(
        verbose_name="Номер отображения"
    )
    is_bool = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name='Поле логическое'
    )
    is_date = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name='Поле дата'
    )
    is_integer = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name='Поле целочисленное'
    )
    is_float = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        verbose_name='Поле с остатком'
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.DO_NOTHING,
        verbose_name="Категория",
        null=False,
        blank=False
    )

    def __str__(self):
        return str(self.key)

    class Meta:
        verbose_name = "Поле описания"
        verbose_name_plural = "Поля описаний"


class ItemDescription(models.Model):
    """ Хранит описание товара по заданному шаблону категории и шаблону описания """
    value = models.TextField(
        default=None,
        verbose_name="Значение параметра",
        null=True,
        blank=True,
    )
    value_bool = models.BooleanField(
        default=None,
        verbose_name='Логическое',
        null=True,
        blank=True
    )
    value_date = models.DateField(
        default=None,
        verbose_name='Дата',
        null=True,
        blank=True
    )
    value_integer = models.IntegerField(
        default=None,
        verbose_name='Число целое',
        null=True,
        blank=True
    )
    value_float = models.FloatField(
        default=None,
        verbose_name='Число плавающее',
        null=True,
        blank=True
    )
    # category = models.ForeignKey(
    #     ItemDescriptionTemplate,
    #     on_delete=models.CASCADE,
    #     verbose_name="Связанный шаблон категории",
    #     null=False,
    #     blank=False
    # )
    item_description_template = models.ForeignKey(
        ItemDescriptionTemplate,
        default=None,
        on_delete=models.CASCADE,
        verbose_name="Связанный шаблон",
        null=False,
        blank=True,
        related_name='description_template'
    )
    item = models.ForeignKey(
        Item,
        on_delete=models.CASCADE,
        verbose_name="Связанный предмет",
        null=False,
        blank=False,
        related_name='item_set'
    )

    class Meta:
        verbose_name = "Заполненное поле описания"
        verbose_name_plural = "Заполенные поля описаний"
