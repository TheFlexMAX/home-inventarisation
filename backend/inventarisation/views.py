import datetime
import json

from django import forms
from django.db import models
from django.db.models import Q
from django.shortcuts import get_object_or_404, render
from rest_framework import viewsets, generics, status
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.filters import SearchFilter, OrderingFilter
from .forms import ItemForm, ItemParams
from .filters import ItemFilter

from .models import (
    Building,
    Room,
    Category,
    Container,
    Item,
    ItemPhoto,
    ItemDescriptionTemplate,
    ItemDescription,
)
from .serializers import (
    BuildingSerializer,
    RoomSerializer,
    CategorySerializer,
    ContainerSerializer,
    ItemListSerializer,
    ItemDetailedSerializer,
    ItemPhotoSerializer,
    ItemDescriptionTemplateSerializer,
)


class BuildingViewSet(viewsets.ViewSet):
    """
    Методы для работы с запросами к модели Building
    """

    def list(self, request):
        """
        Собирает список строений, которые занесены в приложение.
        :param request:
        :return:
        """
        buildings = Building.objects.all()
        serializer = BuildingSerializer(buildings, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Возвращает подробные данные о запрошенном доме.
        :param request:
        :param pk:
        :return:
        """
        """
        Отправляет данные по одному строению
        :param request:
        :param pk:
        :return:
        """
        buildings = Building.objects.all()
        building = get_object_or_404(buildings, pk=pk)
        serializer = BuildingSerializer(building)
        return Response(serializer.data)

    def rooms(self, request, pk=None):
        """
        Возвращает список комнат, находящийся в недвижимости
        :param request:
        :param pk:
        :return:
        """
        building = Building.objects.get(pk=pk)
        rooms = Room.objects.filter(building=building)
        serializer = RoomSerializer(rooms, many=True)
        return Response(serializer.data)

    def items(self, request, pk=None):
        """
        Возвращает список предметов находящийся в недвижимости.
        :param request:
        :param pk:
        :return:
        """
        building = Building.objects.get(pk=pk)
        items = Item.objects.in_building(building=building)
        serializer = ItemListSerializer(items, many=True)
        return Response(serializer.data)


class RoomViewSet(viewsets.ViewSet):
    """
    Методы для работы с запросами к модели Room
    """

    def list(self, request):
        """
        Собирает список комнат, которые занесены в приложение
        :param request:
        :return:
        """
        rooms = Room.objects.all()
        serializer = RoomSerializer(rooms, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Отправляет данные по одной комнате
        :param request:
        :param pk:
        :return:
        """
        rooms = Room.objects.all()
        room = get_object_or_404(rooms, pk=pk)
        serializer = RoomSerializer(room)
        return Response(serializer.data)

    def items(self, request, pk=None):
        """
        Возвращает список предметов в зависимости от выбранной комнаты.
        :param request:
        :param pk:
        :return:
        """
        room = Room.objects.get(pk=pk)
        items = Item.objects.in_room(room=room)
        serializer = ItemListSerializer(items, many=True)
        return Response(serializer.data)

    def containers(self, request, pk=None):
        room = Room.objects.get(pk=pk)
        items = Item.objects.containers_in_room(room)
        serializer = ItemListSerializer(items, many=True)
        return Response(serializer.data)

    def itemsInRoomAndContainers(self, request, pk=None):
        '''
        Возвращает список предметов, которые лежат в переданнх контейнерах
        По переданному ключу комнаты
        '''
        received_containers = request.data.get('containers')
        received_containers_pks = []
        # Выделяем ключи из переданного набора контейнеров
        for container in received_containers:
            received_containers_pks.append(container.get('pk'))
        # Получить список переданных параметров
        room = Room.objects.get(pk=pk)
        containers = Item.objects.filter(room=room, is_container=True, pk__in=received_containers_pks)
        # TODO: Сделать по фиксированным контейнерам позже
        # containers_fixed = Container.objects.filter(room=room, pk__in=received_containers_pks)
        # items = Item.objects.filter(Q(room=room, container_item__in=containers) |
        #                             Q(room=room, container__in=containers))
        # Запрашиваем предметы, которые лежат в этих контейнерах
        items = Item.objects.filter(room=room, container_item__in=containers)

        serializer = ItemListSerializer(items, many=True)
        return Response(serializer.data)


class CategoryViewSet(viewsets.ViewSet):
    """
    Методы для работы с запросами к модели Category
    """

    def list(self, request):
        """
        Собирает список категорий, которые занесены в приложение
        :param request:
        :return:
        """
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Отправляет данные по одной категории
        :param request:
        :param pk:
        :return:
        """
        categories = Category.objects.all()
        category = get_object_or_404(categories, pk=pk)
        serializer = CategorySerializer(category)
        return Response(serializer.data)

    def detailed(self, request, pk=None):
        """
        Возвращает шаблон подробного описания предмета
        :param request:
        :param pk:
        :return:
        """
        category = Category.objects.get(pk=pk)
        template_description_data = ItemDescriptionTemplate.objects.filter(category=category)
        serializer = ItemDescriptionTemplateSerializer(template_description_data, many=True)
        return Response(serializer.data)


class ContainerViewSet(viewsets.ViewSet):
    """
    Методы для работы с запросами к модели Container
    """

    def list(self, request):
        """
        Собирает список контейнеров, которые занесены в приложение
        :param request:
        :return:
        """
        containers = Container.objects.all()
        serializer = ContainerSerializer(containers, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Отправляет данные по одному контейнеру
        :param request:
        :param pk:
        :return:
        """
        containers = Container.objects.all()
        container = get_object_or_404(containers, pk=pk)
        serializer = ContainerSerializer(container)
        return Response(serializer.data)


class ItemViewSet(viewsets.ViewSet):
    """
    Методы для работы с запросами к модели Item
    """

    def list(self, request):
        """
        Собирает список предметов, которые занесены в приложение
        :param request:
        :return:
        """
        items = Item.objects.all()
        serializer = ItemListSerializer(items, many=True)
        return Response(serializer.data)

    def search(self, request):
        """

        :param request:
        :return:
        """
        data = request.data.get('params')
        searchable_item_name = data.get('name')
        search_bar = data.get('searchBar')
        default = data.get('default')
        if default:
            queryset = Item.objects.all()[:5]
            serializer = ItemListSerializer(queryset, many=True)
        else:
            if search_bar:
                queryset = Item.objects.filter(name__icontains=searchable_item_name)[:6]
            else:
                queryset = Item.objects.filter(name__icontains=searchable_item_name)
            serializer = ItemListSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Отправляет детальные данные по одному предмету
        :param request:
        :param pk:
        :return:
        """
        items = Item.objects.all()
        item = get_object_or_404(items, pk=pk)
        serializer = ItemDetailedSerializer(item)
        return Response(serializer.data)

    def add(self, request):
        """
        Добавляет новый предмет в базу данных
        :param request:
        :return:
        """
        # Получает шаблонные данные предмета
        # Получает динамические данные
        print(f"request.data: {request.data}")
        data = request.data.get('formData')
        data_category = data.get('item_form-category')
        data_building = data.get('item_form-building')
        data_room = data.get('item_form-room')
        data_item_container = data.get('item_form-container')
        data_price = data.get('item_form-price')
        data_count = data.get('item_form-count')
        data_purchase_date = data.get('item_form-purchase_date')

        building = None
        room = None
        item_container = None
        price = 0
        count = 1

        if type(data_category) is dict:
            category = Category.objects.get(pk=data_category.get('pk'))
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if type(data_building) is dict:
            building = Building.objects.get(pk=data_building.get('pk'))
        if type(data_room) is dict:
            room = Room.objects.get(pk=data_room.get('pk'))
        if type(data_item_container) is dict:
            item_container = Item.objects.get(pk=data_item_container.get('pk'))
        if data_price.isdigit():
            price = float(data_price)
        if data_count > 0:
            price = data_price
        else:
            # Нельзя 0 или меньше
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if data_purchase_date:
            purchase_date = datetime.datetime.fromisoformat(data_purchase_date[:-1]).date()
        else:
            purchase_date = None

        # Создаем новую запись предмета
        new_item = Item.objects.create(
            name=data.get('item_form-name'),
            category=category,
            is_container=False,
            container_item=item_container,
            container=None,
            room=room,
            price=price,
            count=count,
            date_purchase=purchase_date
        )

        # Получаем шаблон заполнения его категории и заполяем её
        data_dynamic = data.get('itemListDescription')
        item_descriptions_template = ItemDescriptionTemplate.objects.filter(category=category).order_by("order_number")

        i = 0
        for desc_template in item_descriptions_template:
            if desc_template.is_bool:
                if not (data_dynamic[i] is None or len(data_dynamic[i]) <= 0):
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                        value_bool=data_dynamic[i]
                    )
                else:
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                    )
            elif desc_template.is_date:
                if not (data_dynamic[i] is None or len(data_dynamic[i]) <= 0):
                    date = datetime.datetime.fromisoformat(data_dynamic[i][:-1]).date()
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                        value_date=date
                    )
                else:
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item
                    )
            elif desc_template.is_integer:
                if not(data_dynamic[i] is None or len(data_dynamic[i]) <= 0):
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                        value_integer=data_dynamic[i]
                    )
                else:
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                        value_integer=0
                    )
            elif desc_template.is_float:
                if not(data_dynamic[i] is None or len(data_dynamic[i]) <= 0):
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                        value_float=data_dynamic[i]
                    )
                else:
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                        value_float=0
                    )
            else:
                if not(data_dynamic[i] is None or len(data_dynamic[i]) <= 0):
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                        value=data_dynamic[i]
                    )
                else:
                    new_item_description = ItemDescription.objects.create(
                        item_description_template=desc_template,
                        item=new_item,
                        value=""
                    )
            i += 1

        return Response(status=status.HTTP_201_CREATED)

    def page_next(self, request):
        """
        Передает следующий набор страниц предметов
        :param request:
        :return:
        """
        data = request.data.get('params')
        limit = data.get('limit')
        offset = data.get('offset')
        building = data.get('building')
        room = data.get('room')
        containers = data.get('containers')
        containers_pks = []
        # Выделяем ключи из переданного набора контейнеров
        if containers:
            for container in containers:
                containers_pks.append(container.get('pk'))
        # TODO: Отрефакторить позже
        if building:
            if room:
                room = Room.objects.get(pk=room)
                items = Item.objects.in_room_with_range(
                    room=room,
                    offset=offset,
                    limit=limit
                )
                serializer = ItemListSerializer(items, many=True)
                if containers:
                    containers = Item.objects.filter(
                        room=room,
                        is_container=True,
                        pk__in=containers_pks
                    )
                    items = Item.objects.filter(
                        room=room,
                        container_item__in=containers
                    )[int(offset): int(offset) + int(limit)]
                    serializer = ItemListSerializer(items, many=True)
                return Response(serializer.data)
            else:
                building = Building.objects.get(pk=building)
                items = Item.objects.in_building_with_range(
                    building=building,
                    offset=offset,
                    limit=limit,
                )
                serializer = ItemListSerializer(items, many=True)
                return Response(serializer.data)

        # Запрос поумолчанию
        items = Item.objects.in_range(
            offset=offset,
            limit=limit
        )
        serializer = ItemListSerializer(items, many=True)
        return Response(serializer.data)


class ItemPhotoViewSet(viewsets.ViewSet):
    """
    Методы для работы с запросами к модели ItemPhoto
    """

    def list(self, request):
        """
        Собирает список фотограий, которые занесены в приложение
        :param request:
        :return:
        """
        item_photos = ItemPhoto.objects.all()
        serializer = ItemPhotoSerializer(item_photos, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Отправляет данные по одной фотографии вместе с последней
        :param request:
        :param pk:
        :return:
        """
        item_photos = ItemPhoto.objects.all()
        item_photo = get_object_or_404(item_photos, pk=pk)
        serializer = ItemPhotoSerializer(item_photo)
        return Response(serializer.data)


class ItemDescriptionTemplateViewSet(viewsets.ViewSet):
    """
    Методы для работы с запросами к модели ItemDescriptionTemplate
    """

    def list(self, request):
        """
        Собирает список записей, которые занесены в приложение и
        относится к категории
        :param request:
        :return:
        """
        item_description_templates = ItemDescriptionTemplate.objects.all()
        serializer = ItemDescriptionTemplateSerializer(item_description_templates, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Отправляет данные по одной записи ключ-значение
        :param request:
        :param pk:
        :return:
        """
        item_description_templates = ItemDescriptionTemplate.objects.all()
        item_description_template = get_object_or_404(item_description_templates, pk=pk)
        serializer = ItemDescriptionTemplateSerializer(item_description_template)
        return Response(serializer.data)


""" Временные методы для удобства разработки """


def get_all_fields_from_form(instance):
    """"
    Return names of all available fields from given Form instance.
    Only for modelForm

    :arg instance: Form instance
    :returns list of field names
    :rtype: list
    """

    fields = list(instance().base_fields)

    for field in list(instance().declared_fields):
        if field not in fields:
            fields.append(field)
    return fields


class GetItemAdd(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'profile_detail.html'

    def get(self, request, pk):
        context = {}
        content = {}

        category = Category.objects.get(pk=pk)
        template_description_data = ItemDescriptionTemplate.objects.filter(category=category)

        # Добавить поля из динамически запрашиваемой части
        new_fields = {}
        for param in template_description_data:
            if param.is_bool:
                new_fields[str(param)] = forms.BooleanField()
            elif param.is_date:
                new_fields[str(param)] = forms.DateField()
            elif param.is_integer:
                new_fields[str(param)] = forms.IntegerField()
            elif param.is_float:
                new_fields[str(param)] = forms.FloatField()
            else:
                new_fields[str(param)] = forms.CharField()

        dynamic_item_params_form = type('DynamicItemParamsForm', (ItemParams,), new_fields)

        dynamic_item_form = dynamic_item_params_form(content)
        context['dynamic_item_form'] = dynamic_item_form
        item_form = ItemForm(request.POST or None)
        context['item_form'] = item_form
        context['pk'] = pk
        context['category'] = category

        return render(
            request,
            'item_add.html',
            context
        )

    def post(self, request, pk):
        context = {}
        content = {}

        category = Category.objects.get(pk=pk)
        template_description_data = ItemDescriptionTemplate.objects.filter(category=category)

        # Добавить поля из динамически запрашиваемой части
        new_fields = {}
        for param in template_description_data:
            if param.is_bool:
                new_fields[str(param)] = forms.BooleanField()
            elif param.is_date:
                new_fields[str(param)] = forms.DateField()
            elif param.is_integer:
                new_fields[str(param)] = forms.IntegerField()
            elif param.is_float:
                new_fields[str(param)] = forms.FloatField()
            else:
                new_fields[str(param)] = forms.CharField()

        dynamic_item_params_form = type('DynamicItemParamsForm', (ItemParams,), new_fields)
        dynamic_item_form = dynamic_item_params_form(request.POST)
        context['dynamic_item_form'] = dynamic_item_form
        item_form = ItemForm(request.POST)
        context['item_form'] = item_form
        context['pk'] = pk

        # Если все окей, то создаем необходимые записи
        # TODO: прикрепить валидацию к формам
        # if item_form.is_valid() and dynamic_item_form.is_valid():
        # создаем новый предмет
        new_item = item_form.save()
        form_data = request.POST
        # Создаем динамические поля к созданному предмету
        for param in template_description_data:
            # Создать новое поле записи по шаблону
            item_description = ItemDescription.objects.create(
                value=None,
                value_bool=None,
                value_date=None,
                value_integer=None,
                value_float=None,
                item_description_template=param,
                item=new_item,
            )

            if param.is_bool:
                new_fields[str(param)] = forms.BooleanField()
                item_description.value_bool = form_data.get('item_params-' + str(param))
            elif param.is_date:
                new_fields[str(param)] = forms.DateField()
                item_description.value_date = form_data.get('item_params-' + str(param))
            elif param.is_integer:
                new_fields[str(param)] = forms.IntegerField()
                item_description.value_integer = form_data.get('item_params-' + str(param))
            elif param.is_float:
                new_fields[str(param)] = forms.FloatField()
                item_description.value_float = form_data.get('item_params-' + str(param))
            else:
                new_fields[str(param)] = forms.CharField()
                item_description.value = form_data.get('item_params-' + str(param))

            item_description.save()

        return render(
            request,
            'item_add.html',
            context
        )
