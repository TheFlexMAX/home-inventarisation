from rest_framework import serializers

from .models import (
    Building,
    Room,
    Category,
    Container,
    Item,
    ItemPhoto,
    ItemDescriptionTemplate,
    ItemDescription,
)

"""
    Данный документ необходим для того, чтобы определять,
    в каком виде будут приходить данные на сторону клиента
    (структура и данные json)
"""


class BuildingSerializer(serializers.ModelSerializer):
    """
    Список зданий в программе
    """

    class Meta:
        model = Building
        fields = ('pk', 'name', 'adress')


class RoomSerializer(serializers.ModelSerializer):
    """
    Список комнат
    """
    # Позволяет достать данные об имени здания, а не pk
    building = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = Room
        fields = ('pk', 'name', 'building',)


class CategorySerializer(serializers.ModelSerializer):
    """
    Список категорий
    """

    class Meta:
        model = Category
        fields = ('pk', 'name',)


class ContainerSerializer(serializers.ModelSerializer):
    """
    Список контейнеров
    """
    # Позволяет достать данные об имени здания, а не pk
    building = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = Container
        fields = ('name', 'building',)


class ItemPhotoSerializer(serializers.ModelSerializer):
    """
    Данные о фотографии предмета
    """

    class Meta:
        model = ItemPhoto
        fields = ('photo_file', 'date_added',)


class ItemDescriptionSerializer(serializers.ModelSerializer):
    """
    Сериализирует параметры описания предмета
    """
    item_description_template = serializers.SlugRelatedField(slug_field='key', read_only=True)

    class Meta:
        model = ItemDescription
        fields = ('value', 'value_bool', 'value_date', 'value_integer', 'value_float', 'item_description_template')


class ItemDetailedSerializer(serializers.ModelSerializer):
    """
    Позволяет получить подробные данные о предмете
    """
    category = serializers.SlugRelatedField(slug_field='name', read_only=True)
    container = serializers.SlugRelatedField(slug_field='name', read_only=True)
    room = serializers.SlugRelatedField(slug_field='name', read_only=True)
    photos = ItemPhotoSerializer(many=True)
    parameters = ItemDescriptionSerializer(source='item_set', many=True)

    class Meta:
        model = Item
        fields = ('pk', 'name', 'category', 'is_container', 'container_item', 'container', 'room', 'count', 'photos', 'parameters', 'price',)


class ItemListSerializer(serializers.ModelSerializer):
    """
    Позволяет получить обшие данные о предметах в приложении
    """
    # Позволяет достать данные об имени здания, а не pk
    category = serializers.SlugRelatedField(slug_field='name', read_only=True)
    container = serializers.SlugRelatedField(slug_field='name', read_only=True)
    room = serializers.SlugRelatedField(slug_field='name', read_only=True)
    photos = ItemPhotoSerializer(many=True)

    class Meta:
        model = Item
        fields = ('pk', 'name', 'category', 'is_container', 'container_item', 'container', 'room', 'photos',)


class ItemDescriptionTemplateSerializer(serializers.ModelSerializer):
    """
    Список предметов
    """
    # category = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = ItemDescriptionTemplate
        fields = ('pk', 'key', 'is_bool', 'is_date', 'is_integer', 'is_float', 'order_number',)
