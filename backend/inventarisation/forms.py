from django import forms
from .models import Item


class ItemForm(forms.ModelForm):
    prefix = 'item_form'

    class Meta:
        model = Item
        fields = '__all__'


class ItemParams(forms.Form):
    prefix = 'item_params'
