from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views
from .views import *


urlpatterns = format_suffix_patterns([
    # building api
    path('building/', views.BuildingViewSet.as_view({'get': 'list'})),
    path('building/<int:pk>/', views.BuildingViewSet.as_view({'get': 'retrieve'})),
    path('building/<int:pk>/rooms/', views.BuildingViewSet.as_view({'get': 'rooms'})),
    path('building/<int:pk>/items/', views.BuildingViewSet.as_view({'get': 'items'})),
    # room api
    path('room/', views.RoomViewSet.as_view({'get': 'list'})),
    path('room/<int:pk>/', views.RoomViewSet.as_view({'get': 'retrieve'})),
    path('room/<int:pk>/items/', views.RoomViewSet.as_view({'get': 'items'})),
    path('room/<int:pk>/containers/', views.RoomViewSet.as_view({'get': 'containers'})),
    path('room/<int:pk>/containers/items/', views.RoomViewSet.as_view({'post': 'itemsInRoomAndContainers'})),
    # category api
    path('categories/', views.CategoryViewSet.as_view({'get': 'list'})),
    path('category/<int:pk>/', views.CategoryViewSet.as_view({'get': 'retrieve'})),
    path('category/<int:pk>/description', views.CategoryViewSet.as_view({'get': 'detailed'})),
    # containers api
    path('container/', views.ContainerViewSet.as_view({'get': 'list'})),
    path('container/<int:pk>/', views.ContainerViewSet.as_view({'get': 'retrieve'})),
    # items api
    path('item/', views.ItemViewSet.as_view({'get': 'list'})),
    path('item/<int:pk>/', views.ItemViewSet.as_view({'get': 'retrieve'})),
    path('item/page-next/', views.ItemViewSet.as_view({'post': 'page_next'})),
    path('item/add/', views.ItemViewSet.as_view({'post': 'add'})),
    path('item/search/', views.ItemViewSet.as_view({'post': 'search'})),
    # Items photo
    path('item-photo/', views.ItemPhotoViewSet.as_view({'get': 'list'})),
    path('item-photo/<int:pk>/', views.ItemPhotoViewSet.as_view({'get': 'retrieve'})),
    # custom admin-panel
    path('add-item/<int:pk>/', GetItemAdd.as_view(), name="item_add_admin"),
])
