from django.db import models


class RoomManager(models.Manager):
    """
    Инициализирует расширенные и понятные запросы под систему для моделей комнаты
    """
    pass


class ItemManager(models.Manager):
    """
    Инициализирует расширение запросов для предметов
    """

    def in_range(self, offset, limit):
        """
        Возвращает переданный срез набора предметов. Нужен для бесконечного
        скролла страницы
        :param offset:
        :param limit:
        :return:
        """
        return self.all()[int(offset): int(offset) + int(limit)]

    def in_room(self, room):
        """
        Возвращает набор предметов в комнате
        :param room:
        :return:
        """
        return self.filter(room=room)

    def in_room_with_range(self, room, offset: int, limit: int):
        """
        Возвращает набор предметов в комнате
        :param limit:
        :param offset:
        :param room:
        :return:
        """
        return self.filter(room=room)[int(offset): int(offset) + int(limit)]

    def containers_in_room(self, room):
        """
        Возвращает набор контейнеров в комнате
        :param room:
        :return:
        """
        return self.filter(room=room, is_container=True)

    def in_building(self, building):
        """
        Возвращает набор предметов, которые находятся в переданном здании
        :param building:
        :return:
        """
        return self.filter(room__building=building)

    def in_building_with_range(self, building: models, offset: int, limit: int):
        """
        Возвращает набор предметов, которые находятся в переданном здании
        :param limit:
        :param offset:
        :param building:
        :return:
        """
        return self.filter(room__building=building)[int(offset): int(offset) + int(limit)]
