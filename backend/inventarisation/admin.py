from django.contrib import admin

from .models import (
    Building,
    Room,
    Category,
    Container,
    Item,
    ItemPhoto,
    ItemDescriptionTemplate,
    ItemDescription,
)

# Register your models here.
admin.site.register(Building)
admin.site.register(Room)
admin.site.register(Category)
admin.site.register(Container)
admin.site.register(Item)
admin.site.register(ItemPhoto)
admin.site.register(ItemDescriptionTemplate)
admin.site.register(ItemDescription)
