import {AfterViewInit, Component, ElementRef, Inject, OnInit, PLATFORM_ID} from '@angular/core';

import { Item } from '../../models/item.model';
import { SharedService } from '../../service/shared.service';

import { ItemService } from '../../service/item.service';

import { Subscription } from 'rxjs';
import { Building } from '../../models/building.model';
import { Room } from '../../models/room.model';
import { debounce } from 'lodash';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit, AfterViewInit {

  public items: Item[] = [];
  public building: Building;
  public room: Room;
  public containers: Item[] = [];
  private offset = 0;
  private limit = 20;

  private infinityScrollOn = false; // Необходим для того, чтобы делать запрос по поисковой панели


  // TODO: Исправить загрузку поумолчанию
  // TODO: Сделать сброс количества элементов (offset) при новой загрузке данных
  clickEventSubscriptionNav: Subscription;
  enterEventSubscriptionSearch: Subscription;

  constructor(private _sharedService: SharedService, private _itemService: ItemService) {  }


  ngOnInit(): void {
    // Подписываемся на получение данных в сервисе от навигационной панели
    this.clickEventSubscriptionNav = this._sharedService.getClickNavEvent().subscribe((data) => {
      // При нажатии на навигационном компоненте будут переданы различные данные
      // Проверяем, какие данные приходят на сервис подписки и выполняем различные
      // действия, в зависимости от данных
      if (data.dataType === 'building') {
        // является домом -> запрашивает данные для дома
        this.building = data.building;
        this._itemService.requireBuildingItems(data.building).subscribe(
          receivedItems => {
            this.offset = 0;
            this.items = receivedItems;
          }
        );
      } else if (data.dataType === 'room') {
        // является комнатой -> Запрашиваем данные по комнате
        this.room = data.room;
        this._itemService.requireRoomItems(data.room).subscribe(
          receivedItems => {
            this.offset = 0;
            this.items = receivedItems;
          }
        );
      } else if (data.dataType === 'containers') {
        // является контейнером и комнатой -> Запрашиваем данные в комнате по переданным
        // контейнерам
        this.containers = data.containers;
        if (data.containers.length !== 0) {
          this._itemService.requireItemsByContainers(data.room, data.containers).subscribe(
            receivedItems => {
              this.offset = 0;
              this.items = receivedItems;
            }
          );
        } else {
          // Делаем запрос по комнате
          this._itemService.requireRoomItems(data.room).subscribe(
            receivedItems => {
              this.offset = 0;
              this.items = receivedItems;
            }
          );
        }
      } else {
        // Запрашиваем стандартные данные
        this.offset = 0;
      }
    });

    // Подписывается на получение данных в сервисе от поисковой строки
    this.enterEventSubscriptionSearch = this._sharedService.getEnterSearchEvent().subscribe(
      data => {
        if (data.length <= 0) {
          this.offset = 0;
          this.building = null;
          this.room = null;
          this.containers = [];
          this.items = [];
          this.getNextItems();
          // TODO: Сделать сброс навигационной панели
        } else {
          this._itemService.requireItemsByName(data).subscribe(
            items => {
              this.offset = 0;
              this.infinityScrollOn = false;
              this.building = null;
              this.room = null;
              this.containers = [];
              this.items = [];
              this.items = items;
              this.offset += this.items.length;
              // TODO: Сделать сброс навигационной панели
            }
          );
        }
      },
      err => console.log(err),
      () => {}
    );
    // TODO: Найти другой бесконечный скролл
  }

  ngAfterViewInit(): void {
    this.getNextItems();
  }

  getNextItems(): void {
    this._itemService.requirePaginationNext(this.offset, this.limit, this.building, this.room, this.containers).subscribe(
      data => {
        this.infinityScrollOn = false;
        if (data.length > 0) {
          let item;
          for (item of data) {
            this.items.push(item);
          }
          this.offset += this.limit;
        }
      },
      err => console.log(err),
      () => {
        this.infinityScrollOn = true;
        console.log('done loading pagination next');
      }
    );
  }

  getNextItemsOnScroll(): void {
    // Получает набор следующего набора предметов из сервера когда прокрутка предметов в конце
    // Проверка на то, что поисковая строка пуста. Если пуста - обычный запрос, если не пуста - игнорируем
    if (this.infinityScrollOn) {
      this.getNextItems();
    } else {
      // TODO: сделать пагинацию с именем предмета. Нужна для того, когда результатов очень много
    }
  }

  itemClicked = (item) => {
    // Свое событие нажатия по элементу, которое сообщает подписчикам расшаренного сервиса о действии
    this._sharedService.sendClickEvent(item);
  }
}
