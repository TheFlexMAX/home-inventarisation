import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import { NavPanelService } from '../../service/nav-panel.service';
import { Building } from '../../models/building.model';
import { Room } from '../../models/room.model';
import { Item } from '../../models/item.model';
import { AddItemService } from '../../service/add-item.service';
import { Category } from '../../models/category.model';
import { ItemParameters } from '../../models/item-parameters.model';


@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['../inventarisation/inventarisation.component.scss', './add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  addItemForm: FormGroup;
  submitted = false;
  categories: Category[];
  buildings: Building[];
  rooms: Room[] = [];
  containers: Item[];
  photos: [] = [];
  selectedCategory: Category;
  selectedBuilding: Building;
  selectedRoom: Room;
  dynamicFieldsData: ItemParameters[];
  itemListDescription: FormArray = new FormArray([]);
  dynamicIsVisible = false;
  photosIsVisible = false;

  constructor(private _addItemService: AddItemService) { }

  ngOnInit(): void {
    this.addItemForm = new FormGroup({
      // Общие данные
      'item_form-category': new FormControl('', [
        Validators.required,
      ]),
      'item_form-name': new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      'item_form-purchase_date': new FormControl(''),
      'item_form-count': new FormControl('', [
        Validators.required,
        Validators.max(1000000000),
        Validators.min(1)
      ]),
      'item_form-price': new FormControl('', [
        Validators.required,
        Validators.max(9999999999),
        Validators.min(0)
      ]),
      // Местоположение
      'item_form-building': new FormControl('', [
        Validators.required
      ]),
      'item_form-room': new FormControl(''),
      // TODO: сделать возможность выбирать тип контейнера
      'item_form-container': new FormControl(''),
      itemListDescription: new FormArray([]),
    });
    this.getCategories();
    this.getBuildings();
    // TODO: Генерировать подродные сведения
    // TODO: Добавить возможность прикреплять фото
  }

  onSubmit(): void {
    /* Отправка данных из формы на сервер после её заполнения */
    // FIXME: костыль с запазданием записи arrayForm из формы
    if (this.addItemForm.invalid) {
      return;
    }

    const data = this.addItemForm.value;
    data.itemListDescription = this.addItemForm.controls.itemListDescription.value;
    this._addItemService.addNewItem(data).subscribe();
  }

  getCategories(): void {
    /* Запрашивает список категорий для выпадающего списка */
    this._addItemService.requireCategoryList().subscribe(
      data => {
        this.categories = data;
      },
      err => console.log(err),
      () => console.log('done loading categories')
    );
  }

  getBuildings(): void {
    /* Запрашивает список строений для выпадающего списка */
    this._addItemService.requireBuildingList().subscribe(
      data => {
        this.buildings = data;
      },
      err => console.log(err),
      () => console.log('done loading buildings')
    );
  }

  getRooms(building: Building): void {
    /* Запрашивает список комнат для выпадающего списка */
    this._addItemService.requireRooms(building).subscribe(
      data => {
        this.rooms = data;
      },
      err => console.log(err),
      () => console.log('done loading rooms for building')
    );
  }

  getContainers(room: Room): void {
    /* Запрашивает список подвижных и неподвижных контейнеров для выпадающего списка */
    this._addItemService.requireContainers(room).subscribe(
      data => {
        this.containers = data;
      },
      err => console.log(err),
      () => console.log('done loading container for rooms')
    );
  }

  getDynamicFieldsByCategory(selectedCategory: Category): void {
    /* Запрашивает динамическую часть полей формы */
    if (selectedCategory){
      this._addItemService.requireFieldsAddItemForm(selectedCategory).subscribe(
        data => {
          this.dynamicFieldsData = data;
          this.generateFieldsForm();
        },
        err => console.log(err),
        () => console.log('done loading dynamicFieldsData')
      );
    } else {
      this.resetDynamicFieldsForm();
    }
  }

  resetDynamicFieldsForm(): void {
    /* Пересоздает динамическую часть массива контроллеров */
    this.itemListDescription = new FormArray([]);
    this.addItemForm.controls.itemListDescription = new FormArray([]);
    this.dynamicIsVisible = false;
  }

  generateFieldsForm(): void {
    /* Для генерации динамических полей формы */
    this.resetDynamicFieldsForm();
    if (this.dynamicFieldsData) {
      const curClass = this;
      let i = 0;
      this.dynamicIsVisible = true;
      this.dynamicFieldsData.forEach(field => {
        curClass.itemListDescription.push(new FormControl());
        i++;
      });
      this.addItemForm.controls.itemListDescription = this.itemListDescription;
    }

    console.log('this.addItemForm.controls.itemListDescription');
    console.log(this.addItemForm.controls.itemListDescription);
    console.log('this.itemListDescription');
    console.log(this.itemListDescription);
  }

  resetFieldsValuesForm(): void {
    // TODO: Очищает array form в форме для новых полей значений
  }

  onCategoryChange(selectedCategory: Category): void {
    // Для запуска механизма запроса и генерации динамической части формы
    this.selectedCategory = selectedCategory;
    this.getDynamicFieldsByCategory(selectedCategory);
  }

  onBuildingChange(selectedBuilding: Building): void {
    // Запрашивавет данные о комнатах и производит очистку старых комнат при смене строения
    // В том числе очищает старые контейнеры
    // Сохраняем нашу выбранную недвижимость
    this.selectedBuilding = selectedBuilding;
    // Сбросить связанные данные
    this.resetContainers();
    this.resetRooms();
    if (selectedBuilding) {
      // Запросить комнаты для выбранного дома
      this.getRooms(this.selectedBuilding);
    }
    // Сообщает подписчикам, что данные изменились
    // this.selectedBuildingSubject.next(selectedBuilding);
  }

  onRoomChange(selectedRoom: Room): void {
    // Запрашивавет данные о котнейнерах и производит очистку старых контейнеров при смене строения
    // Сохраняем нашу выбранную комнату
    this.selectedRoom = selectedRoom;
    // Сбросить связанные данные
    this.resetContainers();
    if (selectedRoom) {
      // Запросить контейнеры для выбранной комнаты
      this.getContainers(this.selectedRoom);
    }
  }

  resetRooms(): void {
    // Сбрасывает данные по комнатам при сбросе выбранного здания
    this.rooms = null;
    // this.selectedRoom = null;
  }

  resetContainers(): void {
    // Сбрасывает данные по контейнерам
    this.containers = null;
    // this.selectedContainers = null;
  }
}
