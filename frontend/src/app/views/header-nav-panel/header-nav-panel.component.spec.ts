import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderNavPanelComponent } from './header-nav-panel.component';

describe('HeaderNavPanelComponent', () => {
  let component: HeaderNavPanelComponent;
  let fixture: ComponentFixture<HeaderNavPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderNavPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderNavPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
