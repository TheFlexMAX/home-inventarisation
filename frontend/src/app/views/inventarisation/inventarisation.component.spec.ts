import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventarisationComponent } from './inventarisation.component';

describe('InventarisationComponent', () => {
  let component: InventarisationComponent;
  let fixture: ComponentFixture<InventarisationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventarisationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventarisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
