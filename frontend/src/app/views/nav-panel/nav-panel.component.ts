import { Component, OnInit } from '@angular/core';
import {Building} from '../../models/building.model';
import {Room} from '../../models/room.model';
import {NavPanelService} from '../../service/nav-panel.service';
import {Item} from '../../models/item.model';
import {Observable, Subject, Subscription} from 'rxjs';
import {SharedService} from '../../service/shared.service';

@Component({
  selector: 'app-nav-panel',
  templateUrl: './nav-panel.component.html',
  styleUrls: ['./nav-panel.component.scss']
})
export class NavPanelComponent implements OnInit {

  buildings: Building[];
  rooms: Room[] = [];
  containers: Item[];
  selectedBuilding: Building;
  selectedRoom: Room;
  selectedContainers: Item[];
  panelOpenState = false;

  // selectedBuildingSubject = new Subject<Building>();
  // selectedRoomSubject = new Subject<Room>();
  // selectedContainersSubject = new Subject<Item[]>();

  constructor(private _navPanelService: NavPanelService, private _sharedService: SharedService) { }

  ngOnInit(): void {
    // Посылаем запрос на новые данные о зданиях
    this.getBuildings();
  }


  getBuildings(): void {
    // Получает все строения, имеющиеся в БД
    this._navPanelService.requireBuildingList().subscribe(
      data => {
        this.buildings = data;
      },
      err => console.log(err),
      () => console.log('done loading buildings')
    );
  }

  getRooms(building: Building): void {
    // Получает все комнаты в выбранном помещении
    this._navPanelService.requireRooms(building).subscribe(
      data => {
        this.rooms = data;
      },
      err => console.log(err),
      () => console.log('done loading rooms for building')
    );
  }

  getContainers(room: Room): void {
    // Получает контейнеры в выбранной комнате
    this._navPanelService.requireContainers(room).subscribe(
      data => {
        this.containers = data;
      },
      err => console.log(err),
      () => console.log('done loading container for rooms')
    );
  }

  onSelectedBuildingChange(selectedBuilding: Building): void {
    // Запрашивавет данные о комнатах и производит очистку старых комнат при смене строения
    // В том числе очищает старые контейнеры
    // Сохраняем нашу выбранную недвижимость
    this.selectedBuilding = selectedBuilding;
    // Сбросить связанные данные
    this.resetContainers();
    this.resetRooms();
    if (selectedBuilding) {
      // Запросить комнаты для выбранного дома
      this.getRooms(this.selectedBuilding);
      // Посылаем уведомление о событии и запрашиваем список предметов
      this._sharedService.sendClickNavDropdownsEvent(selectedBuilding, 'building');
    } else {
      // Запросить стандартно
      this._sharedService.sendClickNavDropdownsEvent('none', 'none');
    }
    // Сообщает подписчикам, что данные изменились
    // this.selectedBuildingSubject.next(selectedBuilding);
  }

  onSelectedRoomChange(selectedRoom: Room): void {
    // Запрашивавет данные о котнейнерах и производит очистку старых контейнеров при смене строения
    // Сохраняем нашу выбранную комнату
    this.selectedRoom = selectedRoom;
    // Сбросить связанные данные
    this.resetContainers();
    if (selectedRoom) {
      // Запросить контейнеры для выбранной комнаты
      this.getContainers(this.selectedRoom);
      this._sharedService.sendClickNavDropdownsEvent(this.selectedRoom, 'room');
    } else {
      // Запросить данные для выбранного дома
      this._sharedService.sendClickNavDropdownsEvent(this.selectedBuilding, 'building');
    }
    // Сообщаем о том, что выбранная комната измениласб
    // this.selectedRoomSubject.next(selectedRoom);

    // TODO: Запросить новый список предметов
  }

  onSelectedContainerChange(selectedContainers: Item[]): void {
    this.selectedContainers = selectedContainers;
    if (selectedContainers) {
      // Сохраняем наш выбранный контейнер
      this.selectedContainers = selectedContainers;
      // TODO: запросить новый список предметов
      const room = this.selectedRoom;
      const containers = selectedContainers;
      const data = {
        room,
        containers
      };
      this._sharedService.sendClickNavDropdownsEvent(data, 'containers');
    }
    // Сообщаем о том, что был выбран новый контейнер
    // this.selectedContainersSubject.next(selectedContainer);
  }

  resetRooms(): void {
    // Сбрасывает данные по комнатам при сбросе выбранного здания
    this.rooms = null;
    this.selectedRoom = null;

    // Сообщаем, что данные поля пусты
    // this._navPanelService.roomsSubject.next();
    // this.selectedRoomSubject.next();
  }

  resetContainers(): void {
    // Сбрасывает данные по контейнерам
    this.containers = null;
    this.selectedContainers = null;

    // Сообщаем о том, что данные поля пусты
    // this._navPanelService.containersSubject.next();
    // this.selectedContainersSubject.next();
  }
}
