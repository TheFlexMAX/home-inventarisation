import {Component, ElementRef, HostListener, OnInit, } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

import {Item} from '../../models/item.model';

import {ItemPanelService} from '../../service/item-panel.service';
import { SharedService } from '../../service/shared.service';

import { Subscription } from 'rxjs';


@Component({
  selector: 'app-item-panel',
  templateUrl: './item-panel.component.html',
  styleUrls: ['./item-panel.component.scss']
})
export class ItemPanelComponent implements OnInit {

  isHidden: boolean;
  clickEventsubscription: Subscription;
  public item: Item;

  constructor(private _itemPanelService: ItemPanelService, private sharedService: SharedService, private elRef: ElementRef) {
    this.isHidden = true;
    this.clickEventsubscription = this.sharedService.getClickEvent().subscribe((item) => {
      this.getDetailedItem(item.pk);
    });
  }

  ngOnInit(): void {
  }

  showPanel(): void {
    // Отображает панель визуально
    this.isHidden = false;
  }

  hidePanel(): void {
    // Скрывает визуально панель
    this.isHidden = true;
  }

  public getDetailedItem(id: number): void {
    // Получает подробные данные о желаемом предмете
    this._itemPanelService.one(id).subscribe(
      data => {
        // заменить пустые даты на строки с текстов неизвестно
        data.parameters.forEach(param => {
          if ((param.value === '' || param.value == null) && param.value_date == null) {
            param.value = 'Неизвестно';
          }
        });
        // Заменить пустые строки на строки с текстом неизвестно
        this.item = data;
        this.showPanel();
      },
      err => console.error(err),
      () => console.log('done loading item')
    );
  }

  @HostListener('document:click', ['$event'])
  clickout(event): void {
    // Создает событие проверки было ли нажата внутри компонента кнопка
    if (this.elRef.nativeElement.contains(event.target)) {
      this.isHidden = false;
    } else {
      this.isHidden = true;
    }
  }

}
