import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {Item} from '../../models/item.model';
import {SearchBarService} from '../../service/search-bar.service';
import { debounce } from 'lodash';
import {SharedService} from '../../service/shared.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  searchControl = new FormControl();
  items: Item[] = [];
  publishSearch = new Subject(); // Расширивает введенный текст в поисковой строке подписчикам
  inputStr  = '';
  searchIsDone = false;

  lastKeypress = 0; // Timestamp в милисекундах

  constructor(private _searchBarService: SearchBarService, private _sharedService: SharedService) { }

  ngOnInit(): void {
    // Инициализирует поисковую строку первыми значениями поумолчанию
    this.queryInit();

    // Сообщаем о том, что метод должен выполняться с задержкой
    this.search = debounce(this.search, 200);
  }

  displayFn(item: Item): string {
    // Позволяет отображать текст в поисковой строке в читаемом формате
    return item && item.name ? item.name : '';
  }

  queryInit(): void {
    // Для запроса данных выпадающего списка поумолчанию
    this._searchBarService.getItemsFirst().subscribe(
      data => {
        this.items = data !== null ? data : [new Item()];
      },
      err => console.log(err),
      () => console.log('done load first items in search bar')
    );
  }

  queryDataByName(name: string): void {
    this._searchBarService.getItems(name).subscribe(
      items => {
        this.items = items;
      },
      err => console.log(err),
      () => console.log('done load new data in search bar')
    );
  }

  updateItemsList(event): void {
    // Отправляет сообщение о том, что пользователь хочет найти предмет по нажатию на ентер
    // Если строка ввода пустая - выводится обычный запрос
    if (this.inputStr.length > 0) {
      this._sharedService.sendEnterSearchEvent(this.inputStr);
      this.searchIsDone = true;
    }
  }

  search($event, item?: Item): void{
    // Функция, совершающая запрос и уведомление об изменении поисковой строки
    let q;
    if (item){
      q = item.name;
      this.inputStr = q;
      this.updateItemsList(event);
    } else {
      q = $event.target.value;
      this.inputStr = q;
    }
    if (q.length > 0) {
      this.queryDataByName(q);
      this.searchIsDone = true;
    } else if (q.length === 0 ) {
      if (this.searchIsDone) {
        this.queryInit();
        this._sharedService.sendEnterSearchEvent('');
        this.searchIsDone = false;
      }
    }
  }
}
