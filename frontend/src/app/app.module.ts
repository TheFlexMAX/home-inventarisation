import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavPanelComponent } from './views/nav-panel/nav-panel.component';
import { HeaderNavPanelComponent } from './views/header-nav-panel/header-nav-panel.component';
import { SearchBarComponent } from './views/search-bar/search-bar.component';
import { SearchFilterComponent } from './views/search-filter/search-filter.component';
import { ItemPanelComponent } from './views/item-panel/item-panel.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import { ItemsListComponent } from './views/items-list/items-list.component';
import { PhotoviewerComponent } from './views/photoviewer/photoviewer.component';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AddItemComponent } from './views/add-item/add-item.component';
import { InventarisationComponent } from './views/inventarisation/inventarisation.component';
import { MatInputModule } from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputCounterModule} from '@angular-material-extensions/input-counter';
import {MatMenuModule} from '@angular/material/menu';
import { InfiniteScrollComponent } from './views/infinite-scroll/infinite-scroll.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    AppComponent,
    NavPanelComponent,
    HeaderNavPanelComponent,
    SearchBarComponent,
    SearchFilterComponent,
    ItemPanelComponent,
    ItemsListComponent,
    PhotoviewerComponent,
    AddItemComponent,
    InventarisationComponent,
    InfiniteScrollComponent,
  ],
    imports: [
        HttpClientModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatListModule,
        MatSelectModule,
        FormsModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        MatInputCounterModule,
        MatMenuModule,
        MatAutocompleteModule,
        InfiniteScrollModule
    ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
