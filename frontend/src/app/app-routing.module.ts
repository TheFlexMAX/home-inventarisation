import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {InventarisationComponent} from './views/inventarisation/inventarisation.component';
import {AddItemComponent} from './views/add-item/add-item.component';


const routes: Routes = [
  { path: '', component: InventarisationComponent },
  { path: 'add-item', component: AddItemComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
