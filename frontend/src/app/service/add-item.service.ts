import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Building } from '../models/building.model';
import { Room } from '../models/room.model';
import { Item } from '../models/item.model';
import { HttpClient } from '@angular/common/http';
import {Category} from '../models/category.model';
import {ItemParameters} from '../models/item-parameters.model';

@Injectable({
  providedIn: 'root'
})
export class AddItemService {

  baseurl: string;

  constructor(private http: HttpClient) {
    this.baseurl = 'http://127.0.0.1:8000';
  }

  requireBuildingList(): Observable<Building[]> {
    return this.http.get<Building[]>(this.baseurl + '/api/inventarisation/V1/building/');
  }

  requireRooms(building: Building): Observable<Room[]> {
    // Возвращает список комнат в переданном доме
    return this.http.get<Room[]>(this.baseurl + '/api/inventarisation/V1/building/' + building.pk + '/rooms/');
  }

  requireContainers(room: Room): Observable<Item[]> {
    // Возвращает набор контейнеров (предметов с перемотрам "это контейнер") находящиеся в комнате
    return this.http.get<Item[]>(this.baseurl + '/api/inventarisation/V1/room/' + room.pk + '/containers/');
  }

  requireCategoryList(): Observable<Category[]> {
    // Возвращает список категорий
    return this.http.get<Category[]>(this.baseurl + '/api/inventarisation/V1/categories/');
  }

  requireFieldsAddItemForm(selectedCategory: Category): Observable<ItemParameters[]> {
    // Возвращает описание шаблона предмета
    return this.http.get<ItemParameters[]>(this.baseurl + '/api/inventarisation/V1/category/' + selectedCategory.pk + '/description');
  }

  addNewItem(formData: any): Observable<any> {
    // Отпправляет на сервер данные о новом предмете из формы
    console.log('Данные для отправления');
    console.log(formData);
    return this.http.post<any>(this.baseurl + '/api/inventarisation/V1/item/add/', { formData });
  }
}
