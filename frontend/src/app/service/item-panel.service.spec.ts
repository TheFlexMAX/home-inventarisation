import { TestBed } from '@angular/core/testing';

import { ItemPanelService } from './item-panel.service';

describe('ItemPanelService', () => {
  let service: ItemPanelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemPanelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
