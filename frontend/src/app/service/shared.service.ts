import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, Subject} from 'rxjs';

import {Item} from '../models/item.model';
import {Building} from '../models/building.model';
import {Room} from '../models/room.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http: HttpClient) {
    this.baseurl = 'http://127.0.0.1:8000';
  }

  baseurl: string;
  private subject = new Subject<any>();
  private navSubject = new Subject<any>();
  private searchSubject = new Subject<any>();

  sendClickEvent(item: Item): void {
    // Сообщает подписчикам, при событии, что данные изменились
    this.subject.next(item);
  }
  getClickEvent(): Observable<any>{
    // Расшаривает свои данные как те, на которые можно подписаться
    return this.subject.asObservable();
  }

  /* -------------- Запросы предметов для навигационной панели -------------- */

  sendClickNavDropdownsEvent(data: any, dataType: string): void {
    // Сообщает подписчикам, при событии, что данные изменились
    let dataNext = {};
    if (dataType === 'building'){
      const building = data;
      dataNext = {
        building,
        dataType
      };
    } else if (dataType === 'room') {
      const room = data;
      dataNext = {
        room,
        dataType
      };
    } else {
      const room = data.room;
      const containers = data.containers;
      dataNext = {
        room,
        containers,
        dataType
      };
    }
    this.navSubject.next(dataNext);
  }
  getClickNavEvent(): Observable<any>{
    // Расшаривает свои данные как те, на которые можно подписаться
    return this.navSubject.asObservable();
  }

  /* -------------- Запросы предметов для навигационной панели -------------- */


  /* -------------- Запросы предметов для поисковой панели -------------- */

  sendEnterSearchEvent(name: string): void {
    // Сообщает подписчикам, при событии, что данные изменились
    this.searchSubject.next(name);
  }
  getEnterSearchEvent(): Observable<any> {
    // Позволяет подписаться на отсылаемые данные поисковой панели при нажатии на Enter
    return this.searchSubject.asObservable();
  }
}
