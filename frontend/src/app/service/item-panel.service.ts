import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {Observable, Subject} from 'rxjs';
import {Item} from '../models/item.model';

@Injectable({
  providedIn: 'root'
})
export class ItemPanelService {

  itemSubject = new Subject<Observable<Item>[]>();
  baseurl: string;

  constructor(private http: HttpClient) {
    this.baseurl = 'http://127.0.0.1:8000';
  }

  // Получит данные об одном предмете из сервере
  one(id): Observable<Item> {
    return this.http.get<Item>(this.baseurl + '/api/inventarisation/V1/item/' + id + '/');
  }
}
