import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Building } from '../models/building.model';
import { Room } from '../models/room.model';
import { Item } from '../models/item.model';
import {Container} from '../models/container.model';

@Injectable({
  providedIn: 'root'
})
export class NavPanelService {

  baseurl: string;

  constructor(private http: HttpClient) {
    this.baseurl = 'http://127.0.0.1:8000';
  }

  requireBuildingList(): Observable<Building[]> {
    return this.http.get<Building[]>(this.baseurl + '/api/inventarisation/V1/building/');
  }

  requireRooms(building: Building): Observable<Room[]> {
    // Возвращает список комнат в переданном доме
    return this.http.get<Room[]>(this.baseurl + '/api/inventarisation/V1/building/' + building.pk + '/rooms/');
  }

  requireContainers(room: Room): Observable<Item[]> {
    // Возвращает набор контейнеров (предметов с перемотрам "это контейнер") находящиеся в комнате
    return this.http.get<Item[]>(this.baseurl + '/api/inventarisation/V1/room/' + room.pk + '/containers/');
  }

}
