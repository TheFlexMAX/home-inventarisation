import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Item} from '../models/item.model';

@Injectable({
  providedIn: 'root'
})
export class SearchBarService {

  baseurl: string;

  constructor(private http: HttpClient) {
    this.baseurl = 'http://127.0.0.1:8000';
  }

  getItemsFirst(): Observable<Item[]> {
    // Получает первые, стандартные данные, для инициализации поиска
    const params = {
      default: true,
      name: ''
    };
    return this.http.post<Item[]>(this.baseurl + '/api/inventarisation/V1/item/search/', { params });
  }

  getItems(name: string): Observable<Item[]> {
    const params = {
      searchBar: false,
      default: false,
      name
    };
    return this.http.post<Item[]>(this.baseurl + '/api/inventarisation/V1/item/search/', { params });
  }
}
