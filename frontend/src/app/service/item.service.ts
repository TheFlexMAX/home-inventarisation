import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import { Item } from '../models/item.model';
import {Observable, Subject} from 'rxjs';
import {Building} from '../models/building.model';
import {Room} from '../models/room.model';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  baseurl: string;

  constructor(private http: HttpClient) {
    this.baseurl = 'http://127.0.0.1:8000';
  }

  list(): Observable<Item[]> {
    // Получает список всех предметов из сервера
    return this.http.get<Item[]>(this.baseurl + '/api/inventarisation/V1/item/');
  }

  requireBuildingItems(building: Building): Observable<Item[]> {
    // Возврашает все предметы, которые находятся в здании
    return this.http.get<Item[]>(this.baseurl + '/api/inventarisation/V1/building/' + building.pk + '/items/');
  }

  requireRoomItems(room: Room): Observable<Item[]> {
    // Вовзвращает все предметы, которые находятся в комнате
    return this.http.get<Item[]>(this.baseurl + '/api/inventarisation/V1/room/' + room.pk + '/items/');
  }

  requireItemsByContainers(room: Room, containers: Item[]): Observable<Item[]> {
    // Преобразовываем массив контейнеров в массив ключей контейнеров
    return this.http.post<Item[]>(this.baseurl + '/api/inventarisation/V1/room/' + room.pk + '/containers/items/', {containers});
  }

  requirePaginationNext(offset: number, limit: number, building?: Building, room?: Room, containers?: Item[]): Observable<Item[]> {
    // Получает набор следующих данных предметов для бесконечного скролла данных
    const params = {
      offset,
      limit,
      building: null,
      room: null,
      containers: null,
    };
    if (building) {
      params.building = building.pk;
    }
    if (room) {
      params.room = room.pk;
    }
    if (containers) {
      params.containers = containers;
    }
    return this.http.post<Item[]>(this.baseurl + '/api/inventarisation/V1/item/page-next/', { params });
  }

  requireItemsByName(name: string, searchBar = true): Observable<Item[]> {
    const params = {
      searchBar,
      default: false,
      name
    };
    return this.http.post<Item[]>(this.baseurl + '/api/inventarisation/V1/item/search/', { params });
  }
}
