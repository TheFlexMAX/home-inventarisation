import { Building } from './building.model';

export class Room {
  pk: number;
  name: string;
  building: Building;

  deserialize(input: any): this {
    Object.assign(this, input);
    this.building = input.building.map(building => new Building().deserialize(building));

    return this;
  }
}
