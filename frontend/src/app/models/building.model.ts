import { Deserializable } from './deserializable.model';

export class Building implements Deserializable {
  pk: number;
  name: string;
  adress?: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }

  // constructor(name: string, adress?: string) {
  //   this.name = name;
  //   this.adress = adress;
  // }
}
