export class ItemParameters {
  pk: number;
  is_bool: boolean = null;
  is_date: boolean = null;
  is_float: boolean = null;
  is_integer: boolean = null;
  key: string;
  order_number: number;
}
