import { Building } from './building.model';

export class Container {
  name: string;
  building: Building;

  deserialize(input: any): this {
    Object.assign(this, input);
    this.building = input.building.map(building => new Building().deserialize(building));

    return this;
  }

  // constructor(name: string, building: Building) {
  //   this.name = name;
  //   this.building = Building;
  // }
}
