import { Category } from './category.model';
import { Container } from './container.model';
import { Room } from './room.model';
import { Building } from './building.model';
import { ItemParameters } from './item-parameters.model';

export class Item {
  pk: number = 0;
  name: string;
  category: string;
  is_container: boolean = false;
  container_item?: any;
  container?: any = false;
  room: string ;
  count: number;
  photos: any[];
  parameters: any;

  // deserialize(input: any): this {
  //   Object.assign(this, input);
  //   this.category = input.building.map(category => new Category().deserialize(category));
  //   this.containerItem = input.building.map(containerItem => new Item().deserialize(containerItem));
  //   this.container = input.building.map(container => new Container().deserialize(container));
  //   this.room = input.building.map(room => new Room().deserialize(room));
  //
  //   return this;
  // }
}
